I have lived in the Denver Metro area since 1980. Over the past 40 years I've had the opportunity to watch Denver transform from a cow-town to one of our country’s most desirable places to live.

Address: 6344 S. Sicily Way, Aurora, CO 80016, USA

Phone: 720-530-0453

Website: https://topdenverrealestateagent.com
